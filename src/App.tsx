import React from "react";
import "./App.css";

function App(): JSX.Element {
    return (
        <div className="App">
            <header className="App-header">
                UD CISC275 with React Hooks and TypeScript
            </header>
            <p>
                <img
                    src="https://media4.giphy.com/media/9V8YiWAcXYxT14YXHI/giphy.gif"
                    alt="Bongo cat"
                />
            </p>
        </div>
    );
}

export default App;
